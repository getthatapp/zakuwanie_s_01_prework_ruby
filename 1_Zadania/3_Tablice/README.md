#  Ruby prework - tablice

#### Schemat rozwiązywania zadań:

* wszystkie zadania z tego działu wykonuj w odpowiednich plikach exercise_X.rb,

#### Zadanie 1:

Zdefiniuj pustą nową tablicę `animals`.
Dodaj do niej imiona co najmniej trzech bajkowych zwierząt, które znasz.

#### Zadanie 2:
Zdefiniuj tablicę składającą się z liczb od 1 do 8.
Wypisz przedostatni element (korzystając z konstrukcji z minusem)

#### Zadanie 3:

Zajrzyj do pliku exercise_3.rb, znajdziesz tam zdefiniowaną tablicę:
```ruby
my_array = ["Harry", "Ron", "Hermione"]
```

* wyświetl w konsoli pierwszy element tablicy,
* wyświetl w konsoli ostatni element tablicy.

#### Zadanie 4
Zdefiniuj tablicę składającą się z liter od a do e.
Wypisz te litery połączone znakiem spacji (poprzez użycie metody z tablicy).
Wynikiem działania Twojego programu ma być ciąg:
"a b c d e"