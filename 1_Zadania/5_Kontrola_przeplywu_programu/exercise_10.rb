puts "Podaj liczbę od 1 - 10: "
n = gets.chomp.to_i

if n < 1 || n > 10
  puts "Zła liczba!"
else
  (1..10).each do |i|
    b = i * n
    puts "#{i} * #{n} = #{b}"
  end
end
