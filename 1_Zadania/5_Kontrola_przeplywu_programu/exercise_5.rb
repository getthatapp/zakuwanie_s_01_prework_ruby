puts "Równanie w postaci ax2 + bx + c == 0"
print "Podaj a: "
a = gets.chomp.to_f
print "Podaj b: "
b = gets.chomp.to_f
print "Podaj c: "
c = gets.chomp.to_f
x_1 = 0
x_2 = 0
delta = (b**2)-(4*a*c)
if delta == 0
	puts "Funkcja ma jedno miejsce zerowe: #{-b/(2*a)}"
elsif delta > 0
	puts "x1 = #{(-b-Math.sqrt(delta))/(2*a)}"
	puts "x1 = #{(-b+Math.sqrt(delta))/(2*a)}"
else
	puts "Funkcja nie ma miejsc zerowych"
end
