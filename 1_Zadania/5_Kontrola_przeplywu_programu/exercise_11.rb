(1..100).each do |i|
  n3 = i % 3 == 0
  n5 = i % 5 == 0

  puts case
  when n3 && n5
    "FizzBuzz"
  when n3
    "Fizz"
  when n5
    "Buzz"
  else
    "x"
  end

end
