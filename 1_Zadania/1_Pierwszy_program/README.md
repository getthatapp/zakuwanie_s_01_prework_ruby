#  Ruby prework - Pierwszy program

#### Schemat rozwiązywania zadań:

* wszystkie zadania z tego działu wykonuj w odpowiednich plikach exercise_X.rb,

#### Zadanie 1

Wpisz w pliku exercise_1.rb następującą instrukcję:

```ruby
puts "Twoje imie"
```

Uruchom ten plik Rubyego w konsoli przy pomocy komendy `ruby exercise_1.rb`

#### Zadanie 2

Uruchom interaktywną konsolę Rubyego `irb` i spróbuj do niej wpisać kilka operacji arytmetycznych np:

```ruby
2 + 3;
10 / 4;
30 * 2;
```

Oczywiście po każdej instrukcji wciskaj Enter.
Zauważ, że będąc w konsoli nie musisz używać puts.


#### Zadanie 3

Spróbuj do interaktywnej konsoli `irb` wpisać następujące linie kodu i sprawdź co się wyświetli:

```
"Hello World"
'Hello World'
2
hello
```

Następnie napisz w komentarzu w pliku exercise_3.rb co wypisała konsola pod każdą instrukcją.

#### Zadanie 4

Używając Ruby napisz program, który:

1. pobierze z klawiatury imię użytkownika, 
2. pobierze z klawiatury nazwisko użytkownika,

po czym wyświetli komunikat "Imię Nazwisko jest programistą Rubyego!"

**Przykład**
```
Podaj imię: Yukihiro
Podaj nazwisko: Matsumoto
Yukihiro Matsumoto jest programistą Rubyego!
```

Podpowiedź: zapisuj wpisywane przez użytkownika dane do zmiennych.