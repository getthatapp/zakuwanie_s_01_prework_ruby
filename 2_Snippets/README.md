# Prework Ruby
> Kilka ważnych informacji


Przed przystąpieniem do rozwiązywania zadań przeczytaj poniższe wskazówki:

#### Jak pobrać wartość z klawiatury i przypisać ją do zmiennej?

```ruby
puts "Input someting: "
a = gets.chomp
puts a # Wyświetl wartość zmiennej a
```

#### Jak skonwerować string na liczbę całkowitą?

```ruby
a = "10"
integer = a.to_i
puts integer # Wyświetl wartość zmiennej a
```

#### Jak uzupełnić string korzystając z interpolacji?

```ruby
a = true
string = "Wartość zmiennej a to #{a}"
puts string # Wyświetl wartość zmiennej string
```

#### Jak stworzyć pustą tablicę?

```ruby
array = []
```

#### Jak stworzyć tablicę z danymi?

```ruby
array = [1, 2, 3]
```

#### Jak konkatenować (łączyć) ciągi znaków?

```ruby
string = "Lubię "
string += "placki"
puts string
```

albo

```ruby
string = "Lubię "
string << "placki"
puts string
```

albo

```ruby
string = "Lubię "
string = string + "placki"
puts string
```